package malik;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class DocSplitter {
    private static String DOCM = ".docm";
    private static String DOCX = ".docx";
    private static String DOC = ".doc";
    private static String pathToDir = System.getProperty("java.io.tmpdir");
    private static List<File> splittedFiles = new ArrayList<>();

    public static void splitDocxByHeadings(String pathToDocx, String pathToDirectory) throws IOException, org.apache.poi.openxml4j.exceptions.InvalidFormatException, NoHeadingsException {
        pathToDir = pathToDirectory;
        String pathToTempFolder = System.getProperty("java.io.tmpdir");
        File rest;
        if(pathToDocx.endsWith(".docx")){
            rest = new File(pathToTempFolder + "/rest" + DOCX);
        }
        else if(pathToDocx.endsWith(".docm")) {
            rest = new File(pathToTempFolder + "/rest" + DOCM);
        }
        else {
            rest = new File(pathToTempFolder + "/rest" + DOC);
        }

        File f = new File(pathToDocx);
        FileInputStream fis = new FileInputStream(f);
        XWPFDocument xdoc = new XWPFDocument(OPCPackage.open(fis));
        for (XWPFHeader header : xdoc.getHeaderList()) {
            header.clearHeaderFooter();
        }
        for (XWPFFooter footer : xdoc.getFooterList()) {
            footer.clearHeaderFooter();
        }
        XWPFDocument xdoc2;
        FileInputStream fis2 = new FileInputStream(f);
        xdoc2 = new XWPFDocument(OPCPackage.open(fis2));

        File testNew;
        if(pathToDocx.endsWith(".docx")){
            testNew = new File(pathToTempFolder + "/testnew" + DOCX);

        }
        else if(pathToDocx.endsWith(".docm")) {
            testNew = new File(pathToTempFolder + "/testnew" +  DOCM);
        }
        else {
            testNew = new File(pathToTempFolder + "/testnew" +  DOC);
        }
        OutputStream fosx = new FileOutputStream(testNew);
        xdoc2.write(fosx);
        fosx.close();

        ArrayList<Integer> headingIndexes = findHeadings1(xdoc);
        int x = 1;
        if(headingIndexes.isEmpty()) {
            throw new NoHeadingsException("File has no headings.");
        }
        while(headingIndexes.size() != 1) {
            //удалить все, до нужного заголовка в копии файла
            for (int i = xdoc2.getBodyElements().size() - 1; i >= headingIndexes.get(1); i--){
                xdoc2.removeBodyElement(i);
            }
            //удалить в начальном файле записанный заголовок
            for(int i = headingIndexes.get(1)-1; i >= 0; i--){
                xdoc.removeBodyElement(i);
            }

            //записать контент копии в новый файл
            xdoc2.enforceUpdateFields();//update table of contents
            OutputStream fos2;
            if(pathToDocx.endsWith(".docx")){
                fos2 = new FileOutputStream(pathToDir + "HeadingX" + x + DOCX);
                splittedFiles.add(new File(pathToDir + "HeadingX" + x + DOCX));
            }
            else if(pathToDocx.endsWith(".docm")) {
                fos2 = new FileOutputStream(pathToDir + "HeadingM" + x + DOCM);
                splittedFiles.add(new File(pathToDir + "HeadingM" + x + DOCM));
            }
            else {
                fos2 = new FileOutputStream(pathToDir + "Heading" + x + DOC);
                splittedFiles.add(new File(pathToDir + "Heading" + x + DOC));
            }
            xdoc2.write(fos2);
            fos2.close();
            //записать в копию, полученный контент изначального файла после удаления сохраненного фрагмента

            OutputStream fosrest = new FileOutputStream(rest);
            xdoc.write(fosrest);
            FileInputStream fisrest = new FileInputStream(rest);
            xdoc2 = new XWPFDocument(OPCPackage.open(fisrest));

            headingIndexes = findHeadings1(xdoc);
            System.out.println("Document " + x + " from Heading " + x);
            x++;
        }

        Path pathRest = Paths.get(rest.getAbsolutePath());
        Files.delete(pathRest);
        Path pathTestNew = Paths.get(testNew.getAbsolutePath());
        Files.delete(pathTestNew);
    }

    private static ArrayList<Integer> findHeadings1(XWPFDocument xdoc) {
        XWPFStyles styles = xdoc.getStyles();
        List<XWPFParagraph> xwpfparagraphs = xdoc.getParagraphs();

        ArrayList<Integer> headings1 = new ArrayList<>();
        for (XWPFParagraph xwpfparagraph : xwpfparagraphs) {
            if (xwpfparagraph.getStyleID() != null) {
                String styleid = xwpfparagraph.getStyleID();
                XWPFStyle style = styles.getStyle(styleid);
                if (style != null) {
                    if (style.getName().equalsIgnoreCase("heading 1") || style.getStyleId().equalsIgnoreCase("berschrift 1")) {
                        headings1.add(xdoc.getPosOfParagraph(xwpfparagraph));
                    }
                }
            }
        }
        return headings1;
    }

    public static List<File> getSplittedFiles() {
        return splittedFiles;
    }
}
