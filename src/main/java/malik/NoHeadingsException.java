package malik;

public class NoHeadingsException extends Exception {
    public NoHeadingsException(String s) {
        super(s);
    }
}
