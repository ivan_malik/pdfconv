package malik;

import com.documents4j.api.DocumentType;
import com.documents4j.api.IConverter;
import com.documents4j.job.LocalConverter;

import java.io.*;
import java.util.Arrays;

public class PdfConverter {

    public static void convert(String input, String output) {
        System.out.println("start");
        FileInputStream inputStream;
        try  {
            FileInputStream docxIS = new FileInputStream(new File(input));
            OutputStream outputStream = new FileOutputStream(new File(output));
            IConverter converter = LocalConverter.builder().build();
            converter
                    .convert(docxIS).as(DocumentType.MS_WORD)
                    .to(outputStream).as(DocumentType.PDF)
                    .prioritizeWith(1000).schedule();
            inputStream = new FileInputStream(output);
            converter.shutDown();
            inputStream.close();
            outputStream.close();

        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println(Arrays.toString(e.getStackTrace()));
        }
        System.out.println("done");
    }
}
