package malik;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.xmlbeans.impl.piccolo.io.FileFormatException;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {
    /*
    public static void main(String[] args) throws NoHeadingsException, InvalidFormatException, IOException {
        DocSplitter.splitDocxByHeadings("C:/Users/DEDUSHKA/IdeaProjects/PdfConv/src/main/java/file2.docm", "C:/docs/");

        for (int i = 0; i < DocSplitter.getSplittedFiles().size(); i++) {
            System.out.println("Converting " + DocSplitter.getSplittedFiles().get(i).getAbsolutePath());
            PdfConverter.convert(DocSplitter.getSplittedFiles().get(i).getAbsolutePath(), "C:/docs/" + "title" + (i + 1) + ".pdf");
        }
    }
    */
    public static void main(String[] args) throws IOException, InvalidFormatException {
        String title = "Title";
        String defaultPathToDirectory = new File(System.getProperty("java.io.tmpdir") + "DocSplitterPdfConverter").getAbsolutePath();
        String pathToDirectory = " ";
        String pathToFile = " ";

        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter path to directory for saving files.");
            pathToDirectory = bufferedReader.readLine();
            if(!(Files.isDirectory(Paths.get(pathToDirectory))))
                throw new FileFormatException();
            System.out.println("Your directory is " + pathToDirectory);

            System.out.println("Enter path to your MS Word file.");
            pathToFile = bufferedReader.readLine();
            if(Files.isRegularFile(Paths.get(pathToFile))) {
                if (!(pathToFile.endsWith(".docx") || pathToFile.endsWith(".docm") || pathToFile.endsWith(".doc"))) {
                    throw new FileNotFoundException("File is not MS Word format.");
                }
            }
            else {
                throw new FileNotFoundException("File is not correct.");
            }
            System.out.println("Your file is " + pathToFile);

            System.out.println("Enter title for files.");
            title = bufferedReader.readLine();


        } catch (FileFormatException e) {
            System.out.println("It is not path to directory. Using default directory: " + defaultPathToDirectory);
            pathToDirectory = defaultPathToDirectory;
        } catch (FileNotFoundException e) {
            System.out.println("It is not file. Try again.");
            System.out.println(e.getMessage());
        }

        try {
            DocSplitter.splitDocxByHeadings(pathToFile, pathToDirectory);

            for (int i = 0; i < DocSplitter.getSplittedFiles().size(); i++) {
                System.out.println("Converting " + DocSplitter.getSplittedFiles().get(i).getAbsolutePath());
                PdfConverter.convert(DocSplitter.getSplittedFiles().get(i).getAbsolutePath(), pathToDirectory + title + (i + 1) + ".pdf");
            }
        } catch (NoHeadingsException e) {
            System.out.println(e.getMessage());
            return;
        }

        System.out.println("All work is done!");
    }

}
